import java.util.Scanner;

public class Challenge1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String prompt = args[0];
        System.out.println(prompt);

        System.out.print("Name: ");
        String name = scanner.nextLine();

        System.out.print("Age: ");
        int age = scanner.nextInt();

        System.out.println("Hello "+ name + ", nice to meet you!\nYou are " + age + " years old.");
    }
}
