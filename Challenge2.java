import java.util.Scanner;

public class Challenge2 {

    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        String name = getName();
        printTotal(name);
    }

    public static String getName(){
        System.out.println("What is the student's name? ");
        String name = scanner.nextLine();
        return name;
    }

    public static void printTotal(String name) {
        double total = 0;
        if (livingOnCampus(name)){
            total = getTotal(getLivingExpenses(), getCostOfSupplies(), getNumberOfCreditHours(), getCostPerHour());
        }
        else {
            total = getTotal(getCostOfSupplies(), getNumberOfCreditHours(), getCostPerHour());
        }
        String year = getYear();
        if (year.equalsIgnoreCase("other"))
            System.out.printf("The total cost for "+name+" is $%7.2f", total);
        else
            System.out.printf("The total cost for "+name+" as a "+year+" is $%7.2f", total);
        System.out.println();
    }

    public static boolean livingOnCampus(String name) {
        boolean invalidAnswer = true;
        do {
        System.out.println("Is " + name + " living on campus? (yes/no)");
        String answer = scanner.nextLine();
        if (answer.equalsIgnoreCase("yes"))
            return true;
        else if (answer.equalsIgnoreCase("no"))
            return false;
        else
            System.out.println("Invalid answer!");
        } while (invalidAnswer);
        return false;
    }

    public static double getLivingExpenses(){
        double total = 0;
        try {
        System.out.println("How many weeks will the student live in the campus? ");
        String value = scanner.nextLine();
        int weeksAtCampus = Integer.parseInt(value);

        System.out.println("How much money do you spend weekly in living expenses? ");
        value = scanner.nextLine();
        double weeklyExpenses = Double.parseDouble(value);

        System.out.println("How much will you pay for the room and board per year?");
        value = scanner.nextLine();
        double costOfRoomAndBoard = Double.parseDouble(value);

        total = (weeksAtCampus * weeklyExpenses) + costOfRoomAndBoard;
        }
        catch (Exception e){
            System.out.println("Invalid data type...");
            e.printStackTrace();
        }
        finally {
            return total;
        }
    }

    public static double getCostOfSupplies(){
        System.out.println("Cost of textbooks and supplies during a year: $");
        double costOfSupplies = scanner.nextDouble();
        return costOfSupplies;
    }

    public static int getNumberOfCreditHours(){
        System.out.println("Number of credit hours per year: ");
        int numberOfCreditHours = scanner.nextInt();
        return numberOfCreditHours;
    }

    public static double getCostPerHour(){
        System.out.println("Cost per credit hour: $");
        double costPerHour = scanner.nextDouble();
        scanner.nextLine();
        return costPerHour;
    }

    public static double getTotal(double livingExpenses, double suppliers, int creditHours, double costPerHour) {
        return livingExpenses + suppliers + (creditHours*costPerHour);
    }

    public static double getTotal(double suppliers, int creditHours, double costPerHour){
        return suppliers + (creditHours*costPerHour);
    }

    public static String getYear(){
        System.out.println("What is the year of the student(Freshman, Sophomore, Junior, Senior, Other)?: ");
        String year = scanner.nextLine();
        return year;
    }
}
